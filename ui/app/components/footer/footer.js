'use strict';

angular.module('controllers')
    .controller('FooterCtrl', ['$scope', '$interval', 'InfoService',
    function ($scope, $interval, InfoService) {
        $interval(InfoService.loadInfo, 1000);
        $scope.InfoService = InfoService;
    }]);

angular.module('directives')
    .directive('footer', function () {
        return {
            templateUrl: '/ui/app/components/footer/footer.html',
            controller: 'FooterCtrl'
        };
    });