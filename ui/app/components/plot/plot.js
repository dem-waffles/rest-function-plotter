'use strict';
angular.module('services')
    .factory('PlotService', ['$http',
        function ($http) {
            var points = null;
            var numPoints = null;
            return {
                loadPlot: function (polynomial, start, stop, increment) {
                    var request = {
                        method: 'POST',
                        url: '/rest/calculate',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            polynomial: polynomial,
                            start: parseFloat(start, 2),
                            stop: parseFloat(stop, 2),
                            increment: parseFloat(increment, 2)
                        }
                    };
                    $http(request)
                        .success(function (p) {
                            points = p.points;
                            numPoints = p.numPoints;
                        });
                },
                getPoints: function () {
                    return points;
                },
                getNumPoints: function () {
                    return numPoints;
                }
            };
        }]);
angular.module('controllers')
    .controller('PlotCtrl', ['$scope', 'PlotService',
        function ($scope, PlotService) {
            $scope.PlotService = PlotService;
            $scope.graphWidth = 1000;
            $scope.graphHeight = 1000;
            $scope.originX = $scope.graphWidth/2;
            $scope.originY = $scope.graphHeight/2;
            $scope.zoomFactor = 25;
            $scope.offsetX = 0;
            $scope.offsetY = 0;
            $scope.showGrid = true;
            $scope.axisColor = '#AAAAAA';
            $scope.gridColor = '#666666';
            $scope.functionColor = '#D2492A';
            $scope.backgroundColor = '#555555';
            $scope.getNumber = function(num) {
                return new Array(Math.floor(num));
            };
        }]);

angular.module('directives')
    .directive('plotDir', function () {
        return {
            templateUrl: '/ui/app/components/plot/plot.html',
            controller: 'PlotCtrl'
        };
    });

angular.module('controllers')
    .controller('PlotFormCtrl', ['$scope', 'PlotService',
        function ($scope, PlotService) {
            $scope.PlotService = PlotService;
            $scope.polynomial = 'x^2';
            $scope.start = -10;
            $scope.stop = 10;
            $scope.increment = .1;
            PlotService.loadPlot($scope.polynomial, $scope.start, $scope.stop, $scope.increment);
        }]);

angular.module('directives')
    .directive('plotForm', function () {
        return {
            templateUrl: '/ui/app/components/plot/form.html',
            controller: 'PlotFormCtrl'
        };
    });
