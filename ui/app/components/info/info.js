'use strict';

angular.module('services')
    .factory('InfoService', ['$http',
        function ($http) {
            var info = null;
            return {
                loadInfo: function () {
                    $http.get('/rest/info')
                        .success(function (i) {
                            info = i;
                        });
                },
                getInfo: function () {
                    return info;
                }
            };
        }]);
