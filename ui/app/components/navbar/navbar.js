angular.module('controllers')
    .controller('NavbarCtrl', ['$scope',
        function ($scope) {
        }]);
angular.module('directives')
    .directive('navbar', function () {
        return {
            templateUrl: '/ui/app/components/navbar/navbar.html',
            controller: 'NavbarCtrl'
        };
    });