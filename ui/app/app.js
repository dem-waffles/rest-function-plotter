'use strict';

angular.module('services', [])
angular.module('controllers', ['services'])
angular.module('directives', [])
angular.module('plot', [
    'controllers',
    'directives'
])