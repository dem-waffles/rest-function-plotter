#!/usr/bin/env bash

echo "[ BUILDING FOR CURRENT PLATFORM ]"
go-bindata -pkg config -o server/server/config/bindata.go ui/...
if [ -d deploy/current ]; then
    rm -rf deploy/current
fi
mkdir -p deploy/current
pushd deploy/current
go build ../../server/server/Server.go
popd
