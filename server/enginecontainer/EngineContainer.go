package enginecontainer

import "gopkg.in/gin-gonic/gin.v1"

var engine *gin.Engine

func init() {
	engine = gin.New()
}

func GetEngine() *gin.Engine {
	return engine
}
