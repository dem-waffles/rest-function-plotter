package main

import (
	"rest-function-plotter/server/enginecontainer"
	_ "rest-function-plotter/server/controller/calculate"
	_ "rest-function-plotter/server/server/config"
	_ "rest-function-plotter/server/controller/info"
	_ "rest-function-plotter/server/controller/dev"
)

func main() {
	enginecontainer.GetEngine().Run()
}
