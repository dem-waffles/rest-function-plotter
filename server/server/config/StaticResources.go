package config

import (
	"gopkg.in/gin-gonic/gin.v1"
	"rest-function-plotter/server/enginecontainer"
	"net/http"
)

func init() {
	assetNames := AssetNames();
	router := enginecontainer.GetEngine()
	for _, e := range assetNames {
		asset, err := Asset(e)
		if err != nil {
			panic(err)
		}
		if e[len(e)-4:] == "html" {
			router.GET(e, func(c *gin.Context) {
				c.Writer.WriteHeader(http.StatusOK)
				c.Writer.Write(asset)
			})
		} else {
			router.GET(e, func (c *gin.Context) {
				c.Writer.WriteHeader(http.StatusOK)
				c.Writer.Write(asset)
			})
		}
	}
	router.GET("/", redirectToUI)
}

func redirectToUI(c *gin.Context) {
	asset, err := Asset("ui/index.html")
	if err != nil {
		panic(err)
	}
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(asset)
}
