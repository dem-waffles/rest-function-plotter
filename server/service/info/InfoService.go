package infoservice

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/shirou/gopsutil/mem"
	"os"
)

type InfoResponse struct {
	Pid                      int     `json:"pid"`
	TotalMemoryPercent       float64 `json:"totalMemoryPercent"`
	ThisProcessMemory        uint64 `json:"thisProcessMemory"`
}

func GetInfo() *InfoResponse {
	amountMemory, _ := calculateMemory()
	v, _ := mem.VirtualMemory()

	return &InfoResponse{
		Pid:    os.Getpid(),
		TotalMemoryPercent: v.UsedPercent,
		ThisProcessMemory: amountMemory,
	}
}

func calculateMemory() (uint64, error) {
	f, err := os.Open(fmt.Sprintf("/proc/%d/smaps", os.Getpid()))
	if err != nil {
		return 0, err
	}
	defer f.Close()

	res := uint64(0)
	pfx := []byte("Pss:")
	r := bufio.NewScanner(f)
	for r.Scan() {
		line := r.Bytes()
		if bytes.HasPrefix(line, pfx) {
			var size uint64
			_, err := fmt.Sscanf(string(line[4:]), "%d", &size)
			if err != nil {
				return 0, err
			}
			res += size
		}
	}
	if err := r.Err(); err != nil {
		return 0, err
	}

	return res, nil
}
