package calculateservice

import (
	"github.com/dem-waffles/evaler"
	"rest-function-plotter/server/util"
	"strconv"
	"strings"
	"fmt"
)

type PolynomialCalculateRequest struct {
	Polynomial string  `json:"polynomial"`
	Start      float64 `json:"start"`
	Stop       float64 `json:"stop"`
	Increment  float64 `json:"increment"`
}

type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type PolynomialCalculateResponse struct {
	Points    []*Point `json:"points"`
	NumPoints int      `json:"numPoints"`
}

func CalculateSingleValue(polynomial string, x string) (float64, error) {
	polynomial = strings.Replace(polynomial, "^", "**", -1)
	rat, err := evaler.EvalWithVariables(polynomial, map[string]string{"x": x})
	if err != nil {
		return 0, err
	}
	result := evaler.BigratToFloat(rat)
	return util.RoundPlus(result, 2), nil
}

func CalculateRange(r PolynomialCalculateRequest) (*PolynomialCalculateResponse, error) {
	if r.Increment <= 0 {
		return nil, fmt.Errorf("The value %f is not valid for an increment.", r.Increment)
	}
	retVal := &PolynomialCalculateResponse{
		Points:    make([]*Point, 0),
		NumPoints: 0,
	}
	x := r.Start
	for x <= r.Stop {
		x = util.RoundPlus(x, 2)
		xString := strconv.FormatFloat(x, 'f', 2, 64)
		y, err := CalculateSingleValue(r.Polynomial, xString)
		if err != nil {
			return nil, err
		}
		retVal.Points = append(retVal.Points, &Point{
			X: x,
			Y: y,
		})
		retVal.NumPoints++
		x += r.Increment
	}
	return retVal, nil
}
