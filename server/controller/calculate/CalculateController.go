package calculatecontroller

import (
	"rest-function-plotter/server/service/calculate"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"rest-function-plotter/server/enginecontainer"
)

func init() {
	router := enginecontainer.GetEngine()
	router.POST("/rest/calculate", calculate)
}

func calculate(c *gin.Context) {
	var request calculateservice.PolynomialCalculateRequest
	err := c.Bind(&request)
	if err != nil {
		c.JSON(http.StatusBadRequest, http.StatusInternalServerError)
		return
	}
	result, err := calculateservice.CalculateRange(request)
	if err == nil {
		c.JSON(http.StatusOK, result)
	} else {
		c.JSON(http.StatusBadRequest, http.StatusInternalServerError)
	}
}
