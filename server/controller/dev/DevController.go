package dev

import (
	"rest-function-plotter/server/service/calculate"
	"strconv"
	"gopkg.in/gin-gonic/gin.v1"
	"rest-function-plotter/server/enginecontainer"
	"net/http"
)

func init() {
	router := enginecontainer.GetEngine()
	router.GET("/rest/dev/calculateSingle/:polynomial/:x", calculateSingleValue)
	router.GET("/rest/dev/calculateRange/:polynomial/:start/:stop/:increment", calculateRange)
}

func calculateSingleValue(c *gin.Context) {
	result, err := calculateservice.CalculateSingleValue(c.Param("polynomial"), c.Param("x"))
	if err == nil {
		c.JSON(http.StatusOK, result)
	} else {
		c.JSON(http.StatusBadRequest, err.Error())
	}
}


func calculateRange(c *gin.Context) {
	start, _ := strconv.ParseFloat(c.Param("start"), 64)
	stop, _ := strconv.ParseFloat(c.Param("stop"), 64)
	increment, _ := strconv.ParseFloat(c.Param("increment"), 64)
	r := calculateservice.PolynomialCalculateRequest{
		Polynomial: c.Param("polynomial"),
		Start: start,
		Stop: stop,
		Increment: increment,
	}
	result, err := calculateservice.CalculateRange(r)
	if err == nil {
		c.JSON(http.StatusOK, result)
	} else {
		c.JSON(http.StatusBadRequest, err.Error())
	}
}
