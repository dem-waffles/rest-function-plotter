package infocontroller

import (
	"rest-function-plotter/server/service/info"
	"gopkg.in/gin-gonic/gin.v1"
	"rest-function-plotter/server/enginecontainer"
	"net/http"
)

func init() {
	router := enginecontainer.GetEngine()
	router.GET("/rest/info", getInfo)
}

func getInfo(c *gin.Context) {
	c.JSON(http.StatusOK, infoservice.GetInfo())
}
